(ns docurl.app
  (:require [clojure.string :as str]
            [clojure.set :as set]
            [reagent.core :as r]
            [reagent.dom :as rd]
            [docurl.urlsync :as urlsync]))

(def !state (r/atom {:value ""}))
(def !value (r/cursor !state [:value]))

(defn- clean-url [url]
  (-> url
      (cond-> (str/starts-with? url "https://") (subs 8))))

(def ^:private long->short
  {"developer.mozilla.org/en-US/docs/Web/" "mozilla/"
   "Global_Objects" "_"
   "JavaScript" "js"
   "Reference" "ref"})

(def ^:private short->long (set/map-invert long->short))

(defn- shorten [url] (reduce-kv str/replace url long->short))
(defn- expand  [url] (reduce-kv str/replace url short->long))

(defn root-component []
  [:div
   [:input {:type "text"
            :value @!value
            :on-change (fn [ev]
                         (reset! !value (-> ev .-target .-value clean-url))
                         (urlsync/update-url! @!state))}]])

(defn- mount-root! []
  (rd/render [root-component]
             (.getElementById js/document "app_root")))

(defn init! []
  (urlsync/init! {:url->state    (fn [url]   (let [[_ fragm] (str/split url #"#/")]
                                               {:value (-> (or fragm "")
                                                           expand)}))
                  :state->url    (fn [state] [:hash (-> (:value state)
                                                        shorten)])
                  :on-url-state! (fn [state] (reset! !state state))})
  (mount-root!))

(defn ^:dev/after-load  _after-load [] (init!))
