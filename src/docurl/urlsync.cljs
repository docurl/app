(ns docurl.urlsync)

(defonce !config (atom {:url->state    (fn [url])
                        :state->url    (fn [state])
                        :on-url-state! (fn [state])}))

(defonce _ext-url-change-listener
  (do
    (set! (.-onpopstate js/window)
          (fn [ev]
            (let [{:keys [url->state
                          on-url-state!]} @!config]
              (-> js/document.location.href
                  url->state
                  on-url-state!))))
    true))

(defn update-url!
  [s-next]
  (when-let [url-expr ((:state->url @!config) s-next)]
    (let [[target-k
           url-cmpt] (if (vector? url-expr) url-expr [:href url-expr])]
      (js/history.pushState nil nil (str (when (= :hash target-k)
                                           "#/")
                                         url-cmpt)))))

(defn init! [config]
  (reset! !config config)
  (js/window.dispatchEvent (js/Event. "popstate")))
